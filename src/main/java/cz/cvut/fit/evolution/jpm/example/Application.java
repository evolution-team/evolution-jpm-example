package cz.cvut.fit.evolution.jpm.example;

import cz.cvut.fit.evolution.jpm.example.configuration.JpmConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@ComponentScan
@Import(JpmConfiguration.class)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
//        final Scanner scanner = new Scanner(System.in);
//        while (scanner.hasNextLine()) {
//            final String s = scanner.nextLine();
//            System.out.println(s);
//        }
    }
}

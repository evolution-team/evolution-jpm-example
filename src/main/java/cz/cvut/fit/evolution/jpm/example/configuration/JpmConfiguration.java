package cz.cvut.fit.evolution.jpm.example.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"classpath:database.xml"})
public class JpmConfiguration {

//    @Bean
//    public HibernateCriteriaCreator hibernateCriteriaCreator() {
//        return new HibernateCriteriaCreator();
//    }
//
//    @Bean
//    public StateUtil stateUtil() {
//        return new StateUtil();
//    }
//
//    @Bean
//    public TaskUtil taskUtil() {
//        return new TaskUtil();
//    }

}
